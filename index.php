<?php 
    include('model.php');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste de plantes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
    <!--menu de navigation-->
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark p-4">
            <h5 class="text-white h4">Mes plantes d'interieur</h5>
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link text-light" aria-current="page" href="index.php">Plantes</a>
                </li>
                <li class="nav-item">
                <a class="nav-link text-light" href="ajout_plante.php">Ajouter</a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    </nav>

    <!--contenu site = liste-->
    <div class="container-fluid">
        <div class="d-flex flex-wrap justify-content-around">
            <?php
                for($i = 0; $i < count($plantes); $i++){
                    ?>
                    <div class="card m-3" style="width: 18rem;">
                        <img class="card-img-top" src="<?php echo $plantes[$i]['image_plante']; ?>" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $plantes[$i]['nom_commun']; ?></h5>
                            <p class="card-text"><?php echo $plantes[$i]['nom_latin']; ?></p>
                            <form action="ok_supprime.php" method="post">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo $plantes[$i]['id']; ?>">
                                </div>
                                <button type="submit" class="btn btn-danger">Supprimer</button>
                            </form>
                        </div>
                    </div>
            <?php   }
            ?>
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>