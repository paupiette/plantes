<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Ajouter une plante</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
    <!--menu de navigation-->
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark p-4">
            <h5 class="text-white h4">Mes plantes d'interieur</h5>
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link text-light" aria-current="page" href="index.php">Plantes</a>
                </li>
                <li class="nav-item">
                <a class="nav-link text-light" href="ajout_plante.php">Ajouter</a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    </nav>

    <!--contenu de la page = formulaire d'ajout de produit-->
    <div class="container-fluid mt-5">
        <div class="row d-flex justify-content-center">
            <form action="ok_ajout.php" method="post" class="col-md-6 col-sm-12">
                <div class="form-group mt-3">
                    <label for="exampleInputPassword1">Lien d'image de la plante</label>
                    <input type="text" name="image_plante" class="form-control" id="exampleInputPassword1" placeholder="https://www.plante.fr">
                </div>
                <div class="form-group mt-3">
                    <label for="exampleInputPassword1">Nom de la plante</label>
                    <input type="text" name="nom_commun" class="form-control" id="exampleInputPassword1" placeholder="Pied d'éléphant">
                </div>
                <div class="form-group mt-3">
                    <label for="exampleInputPassword1">Nom latin de la plante</label>
                    <input type="text" name="nom_latin" class="form-control" id="exampleInputPassword1" placeholder="Beaucarnea recurvata">
                </div>
                <button type="submit" class="btn btn-success mt-3">Ajouter</button>
            </form>
        </div>
    </div>
    
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>