<?php include('connect.php')?>

<?php
function recup_info(){
    global $db;

    $requete=$db->query('SELECT * FROM plantes');

    $tuples = $requete->fetchAll();

    $requete->closeCursor();

    return $tuples;
}

$plantes = recup_info();


function supprime($id_plante){
    global $db;

    //requête de suppression des lignes dans la table product_order : clé étrangère
    $requete = "DELETE FROM plantes WHERE id = :id";

    $requetePO = $db->prepare($requete);
    $id = $id_plante;
    $requetePO->bindValue(':id', $id);
    $requetePO->execute();
    $requetePO->closeCursor();
}

if(isset($_POST['id'])){
    $id_plante = $_POST['id'];
    supprime($id_plante);
    unset($_POST['id']);
}

function ajoute($image_plante, $nom_commun, $nom_latin){
    global $db;

    $reqSQL='INSERT INTO plantes (nom_commun, nom_latin, image_plante) 
		VALUES (:nom_commun, :nom_latin, :image_plante) ';
		
    $requete=$db->prepare($reqSQL);
    $resultat=$requete->execute(array(
        'nom_commun'=>$nom_commun, 
        'nom_latin'=>$nom_latin, 
        'image_plante'=>$image_plante
    ));

    $requete->closeCursor();
	
}

if(isset($_POST['image_plante'])){
    $image_plante = $_POST['image_plante']; 
    $nom_commun = $_POST['nom_commun']; 
    $nom_latin = $_POST['nom_latin'];

    ajoute($image_plante, $nom_commun, $nom_latin);

    unset($_POST['image_plante']);
    unset($_POST['nom_commun']);
    unset($_POST['nom_latin']);
}