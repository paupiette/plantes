drop database if exists plante;

create database plante;
grant all on plante.* to 'claire'@'localhost';
flush privileges;

use plante;

create table plantes(
    id int not null auto_increment,
    primary key (id),
    nom_commun varchar(20) not null,
    nom_latin varchar(50) not null,
    image_plante varchar(200) not null
);

insert into plantes (nom_commun, nom_latin, image_plante) values('Aloe Vera', 'Aloe barbadensis','https://www.nostrodomus.fr/wp-content/uploads/2021/07/aloe-vera-plant-1484074843rYR.jpg');
insert into plantes (nom_commun, nom_latin, image_plante) values('Chaine des coeurs', 'Ceropegia woodii', 'https://www.botanix.com/gpc/_media/Image/2019313104337/plante-interieur-ceropegia-chaine-coeur_660x0.jpg');

select * from plantes;